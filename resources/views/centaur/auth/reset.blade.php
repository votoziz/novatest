@extends('Centaur::layout')

@section('title', 'Resend Activation Instructions')
@section('color', 'grey')
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-4 ">
            <div class="card  bg-secondary text-info ">

                <h3 class="card-header">Восстановить пароль</h3>

                <div class="card-body">
                <form accept-charset="UTF-8" role="form" method="post" action="{{ route('auth.password.request.attempt') }}">
                <fieldset>
                    <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
                        <input class="form-control" placeholder="E-mail" name="email" type="text" value="{{ old('email') }}">
                        {!! ($errors->has('email') ? $errors->first('email', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <input name="_token" value="{{ csrf_token() }}" type="hidden">
                    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Помогите!">
                </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
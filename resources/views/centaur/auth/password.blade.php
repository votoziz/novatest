@extends('Centaur::layout')

@section('title', 'Create A New Password')
@section('color', 'grey')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-4 ">
            <div class="card  bg-secondary text-info ">

                <h3 class="card-header">Восстановить пароль</h3>

                <div class="card-body">
                <form accept-charset="UTF-8" role="form" method="post" action="{{ route('auth.password.reset.attempt', $code) }}">
                <fieldset>
                    <div class="form-group  {{ ($errors->has('password')) ? 'has-error' : '' }}">
                        <input class="form-control" placeholder="Пароль" name="password" type="password" value="">
                        {!! ($errors->has('password') ? $errors->first('password', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <div class="form-group  {{ ($errors->has('password_confirmation')) ? 'has-error' : '' }}">
                        <input class="form-control" placeholder="Повторите пароль" name="password_confirmation" type="password" value="">
                        {!! ($errors->has('password_confirmation') ? $errors->first('password_confirmation', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <input name="_token" value="{{ csrf_token() }}" type="hidden">
                    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Сохранить">
                </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
@extends('Centaur::layout')

@section('title', 'Register')

{{--Передача фона--}}
@section('color', 'grey')


@section('content')




    <div class="row justify-content-center">
        <div class="col-md-4 ">
            <div class="card  bg-secondary text-info ">
                <div class="panel-heading">
                    <h3 class="card-header">Регистрация</h3>
                </div>
                <div class="card-body">


                    {!! Form::open(["route" => "auth.register.attempt"]) !!}
                    <div class="form-group">

                        {!! Form::text("email",null, ["class" => "form-control" , "placeholder" => "E-mail"]) !!}
                        {!! ($errors->has('email') ? $errors->first('email', '<p class="text-danger log-error">:message</p>') : '') !!}
                    </div>
                    <div class="form-group">
                        {!! Form::password("password", ["class" => "form-control", "placeholder" => "Пароль"]) !!}
                        {!! ($errors->has('password') ? $errors->first('password', '<p class="text-danger log-error">:message</p>') : '') !!}
                    </div>
                    <div class="form-group">
                        {!! Form::password("password_confirmation", ["class" => "form-control", "placeholder" => "Подтвердите пароль"]) !!}
                        {!! ($errors->has('password_confirmation') ? $errors->first('password_confirmation', '<p class="text-danger log-error">:message</p>') : '') !!}
                    </div>

                    <br>

                    {!! Form::submit( "Регистрация" , ["class" => "btn btn-outline-info btn-lg btn-block"]) !!}
                    {{ Form::close() }}


                </div>
            </div>
        </div>
    </div>


@stop
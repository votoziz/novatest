@extends('Centaur::layout')

@section('title', 'Login')

{{--Передача фона--}}
@section('color', 'white')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-4 ">
            <div class="card bg-secondary text-info ">

                    <h3 class="card-header">Вход</h3>

                <div class="card-body">


                    {!! Form::open(["route" => "auth.login.attempt"]) !!}
                    <div class="form-group">

                        {!! Form::text("email",null, ["class" => "form-control" , "placeholder" => "E-mail"]) !!}
                        {!! ($errors->has('email') ? $errors->first('email', '<p class="text-danger log-error">:message</p>') : '') !!}
                    </div>
                    <div class="form-group">
                        {!! Form::password("password", ["class" => "form-control", "placeholder" => "Пароль"]) !!}
                        {!! ($errors->has('password') ? $errors->first('password', '<p class="text-danger log-error">:message</p>') : '') !!}
                    </div>

                    <div class="form-check mb-2 mr-sm-2 mb-sm-0">
                        <label class="form-check-label">
                            <input class="form-check-input" name="remember" type="checkbox"
                                   value="true" {{ old('remember') == 'true' ? 'checked' : ''}}>
                            Запомнить меня
                        </label>

                        {{--todo отредактировать чекбокс--}}
                        {{--<label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">--}}
                        {{--<input type="checkbox" class="custom-control-input">--}}
                        {{--<span class="custom-control-indicator"></span>--}}
                        {{--<span class="custom-control-description">Remember my preference</span>--}}
                        {{--</label>--}}
                        {{----}}
                    </div>
                    <br>
                    {!! Form::submit( "Войти" , ["class" => "btn btn-outline-info btn-lg btn-block"]) !!}
                    {{ Form::close() }}
                    <a class="text-info" href="{{route('auth.password.request.form')}}">Забыли пароль?</a> <br>
                    <a class="text-info" href="{{route('auth.activation.request')}}">Не пришло письмо с активацией?</a> <br>

                </div>
            </div>
        </div>
    </div>

@stop
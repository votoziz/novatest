@extends('Centaur::layout')

@section('title', 'Roles')
@section('color', 'white')
@section('content')

    <div class="container">



        <div class="row">
            <div class="  col-12 col-md-6">
                <h1>Роли</h1></div>
            <div class=" col-md-6 col-12 ">
                <a class="btn btn-primary btn-lg btn-block" href="{{ route('roles.create') }}">
                    <i class="fas fa-user-plus"></i> Создать новую роль
                </a></div>

            <div class="col-12">
                <br> <br>
            </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Slug</th>
                            <th>Разрешения</th>
                            <th>Опции</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($roles as $role)
                            <tr>
                                <td>{{ $role->name }}</td>
                                <td>{{ $role->slug }}</td>
                                <td>{{ implode(", ", array_keys($role->permissions)) }}</td>
                                <td>
                                    <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-outline-primary btn-block">
                                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                        Изменить
                                    </a>
                                    <a href="{{ route('roles.destroy', $role->id) }}" class="btn btn-outline-danger btn-block text-danger" data-method="delete" data-token="{{ csrf_token() }}">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                        Удалить
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
@stop
<div class="card mt-5">

    <div class="card-body">

        @foreach($entries as $entry)

            {{--todo сделать так, чтобы админы могли удалять и редактировать записи--}}
            <div class="card mb-3">
                <div class="card-header bg-info text-white">{{$entry->username}}

                    @if(Sentinel::check() && Sentinel::inRole('administrator'))
                        <span class="badge badge-success ">  {{$entry->email}}</span>
                    @endif
                    <span class="float-right badge text-warning">{{ date('d.m.Y H:i',strtotime($entry->created_at)) }}</span>
                </div>
                <div class="card-body">
                    {{$entry->text}}
                </div>
            </div>

        @endforeach

        {{ $entries->links() }}

    </div>


</div>
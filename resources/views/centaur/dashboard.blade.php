@extends('Centaur::layout')


@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8">
                <div class="card">
                    <div class="card-header bg-success text-center text-white">Это гостевая книга</div>
                    <div class="card-body">
                        <div class="alert alert-info">  Вы можете оставить здесь свое сообщение</div>
                        {!! Form::open(['route' => ['entries.store'], 'class' => 'needs-validation', 'novalidate']) !!}
                        <div class="form-group">
                            {!! Form::label('username', 'Имя:') !!}
                            {!! Form::text('username', null, ['class' => 'form-control form-control-sm','required']) !!}
                            <div class="invalid-feedback">Обязательное для заполнения</div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Почта:') !!}
                            {!! Form::text('email', null, ['class' => 'form-control form-control-sm','required']) !!}
                            <div class="invalid-feedback">Обязательное для заполнения</div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('text', 'Сообщение:') !!}
                            {!! Form::textarea('text', null, ['class' => 'form-control form-control-sm','required']) !!}
                            <div class="invalid-feedback">Обязательное для заполнения</div>
                        </div>
                        {!! Form::submit( 'Написать',  ["class" =>  "btn btn-success btn-block"]) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">


            <div class="col-12">
                @include('centaur.guestbook.index')
            </div>
        </div>
    </div>



@endsection

 @extends('Centaur::layout')

@section('title', 'Users')

@section('color', 'white')

@section('content')

        <div class="container">

        <div class="row">
            <div class="  col-12 col-md-6">
                <h1>Пользователи</h1></div>
            <div class=" col-md-6 col-12 ">
                <a class="btn btn-primary btn-lg btn-block" href="{{ route('users.create') }}">
                    <i class="fas fa-user-plus"></i> Создать пользователя
                </a></div>

            <div class="col-12">
                <br> <hr><br>
            </div>



            <div class="col-12 row">
                @foreach ($users as $user)

                    <div class="col-xl-3 col-lg-4 col-md-6 col-12" >
                        <div class="card bg-primary text-info md-3">
                            <div class="card-body text-center">
                                <img src="//www.gravatar.com/avatar/{{ md5($user->email) }}?d=mm"
                                     alt="{{ $user->email }}" class="rounded-circle">
                                @if (!empty($user->first_name . $user->last_name))
                                    <h4>{{ $user->first_name . ' ' . $user->last_name}}</h4>
                                    <p>{{ $user->email }}</p>
                                @else
                                    <h4>{{ $user->email }}</h4>
                                @endif


                            <ul class="list-group text-dark">
                                <li class="list-group-item">
                                    @if ($user->roles->count() > 0)
                                        {{ $user->roles->implode('name', ', ') }}
                                    @else
                                        <em>No Assigned Role</em>
                                    @endif
                                </li>
                            </ul>

                                <br>
                                <a href="{{ route('users.edit', $user->id) }}" class="btn btn-outline-info btn-block">

                                    Редактировать
                                </a>
                                <a href="{{ route('users.destroy', $user->id) }}" class="btn btn-outline-danger text-danger  btn-block"
                                   data-method="delete" data-token="{{ csrf_token() }}">

                                    Удалить
                                </a>

                        </div>
                    </div>
                    </div>

                @endforeach
            </div>

        </div>

        {!! $users->render() !!}
        </div>

@stop

<?php

    $vars = Session::all();
    foreach ($vars as $key => $value) {
        switch($key) {
            case 'success':
            case 'error':
            case 'warning':
            case 'info':
                ?>
                <div class="col-12">
                    <div class="alert alert-{{ ($key == 'error') ? 'danger' : $key }} alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>{{ ucfirst($key) }}:</strong> {!! $value !!}
                    </div>
                </div>
                <?php
                Session::forget($key);
                break;

            case 'errors':
                foreach ($value->messages() as $messages):
                    foreach ($messages as $message):?>
                        <div class="col-12">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>{{$message}}</strong>
                            </div>
                        </div>
                    <?php
                    endforeach;
                endforeach;
            default:

        }
    }

?>

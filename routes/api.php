<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('entries', ['as' => 'api.entries.get', 'uses' => 'ApiEntryController@index']);


Route::post('entries/store', ['as' => 'api.entries.store', 'uses' => 'ApiEntryController@store']);


Route::post('login', ['as' => 'api.login', 'uses' => 'Auth\SessionController@apiLogin']);

//Используется единственное число, так как работаем с 1 элементом
Route::post('entry/update', ['as' => 'api.entry.update', 'uses' => 'ApiEntryController@update']);

Route::post('entry/destroy', ['as' => 'api.entry.destroy', 'uses' => 'ApiEntryController@destroy']);
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    protected $connection = 'mysql';
    protected $table = 'entries';

    protected $fillable = [
        'username', 'email',  'text',
    ];
}

<?php

namespace App\Http\Controllers;


use App\Traits\EntryTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class EntryController extends Controller
{
    use EntryTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['entries'] = $this->getAllEntries();

        return view('centaur.dashboard',$data);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'username'  => 'required',
            'email' => 'required|email',
            'text'  => 'required',

        ]);
        if ($validator->fails()) {
            return redirect(route('home'))
                ->withErrors($validator)
                ->withinput();
        } else {
            $json_response = json_encode($input);
            $response = $this->storeEntry($json_response);

            if ($response['danger']){
                return redirect(route('home'))->with('error',$response['response']);
            }

            return redirect(route('home'))->with('success','Запись оставлена');
        }

    }




}

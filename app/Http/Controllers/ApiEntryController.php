<?php

namespace App\Http\Controllers;

use App\Entry;
use App\Traits\EntryTrait;
use Cartalyst\Sentinel\Persistences\EloquentPersistence;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Http\Request;

class ApiEntryController extends Controller
{
    use EntryTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entries = $this->getAllEntries();
        return response()->json($entries);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->storeEntry($request);
        return response()->json($response['response'], $response['status']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (is_object($request)) {
            $request = $request->getContent();
        }
        $input = json_decode($request);


        if (!isset($input->persistence)) {
            $response['response'] = 'invalid fields (must have: persistence)';
            $response['status'] = 400;
        } else {

            $persistences = EloquentPersistence::where('code', $input->persistence)->first();

            if (is_null($persistences)) {
                $response['response'] = 'invalid authenticate';
                $response['status'] = 401;
            } elseif (!EloquentUser::find($persistences->user_id)->inRole('administrator')) {
                $response['response'] = 'not enough rights to edit';
                $response['status'] = 403;
            } else {
                $response = $this->updateEntry($request);
            }
        }
        return response()->json($response['response'], $response['status']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if (is_object($request)) {
            $request = $request->getContent();
        }
        $input = json_decode($request);

        if (!isset($input->persistence)) {
            $response['response'] = 'invalid fields (must have: persistence)';
            $response['status'] = 400;
        } else {

            $persistences = EloquentPersistence::where('code', $input->persistence)->first();

            if (is_null($persistences)) {
                $response['response'] = 'invalid authenticate';
                $response['status'] = 401;
            } elseif (!EloquentUser::find($persistences->user_id)->inRole('administrator')) {
                $response['response'] = 'not enough rights to edit';
                $response['status'] = 403;
            } else {
                //todo разница только в одной строчке, необходимо вынести общий код за методы или что-то придумать.
                $response = $this->deleteEntry($request);
            }
        }
        return response()->json($response['response'], $response['status']);
    }
}

<?php

namespace App\Traits;

use App\Entry;

trait EntryTrait
{

//    Способ получить код авторизованного пользователя
//dd(Sentinel::getPersistenceRepository()->check())
//Sentinel::getPersistenceRepository()->findUserByPersistenceCode($code)
// найти пользователя по коду


//получить код для работы апи после регистрации
//$result = $this->authManager->authenticate($credentials, $remember);
//
//dd(session()->get('cartalyst_sentinel'));

//Sentinel::getPersistenceRepository()->remove($code)

    //Трейт для одновременного использования по api и фреймворком. другой способ - это вызвать api метод в контроллере. Далею локально, так что так удобней.
    public function getAllEntries()
    {
        $entries = Entry::orderBy('id', 'desc')->paginate(15);
        return $entries;
    }


    public function storeEntry($json_response)
    {

        $status = 201;
        $danger = false;
        $response = 'success';

        //рестлер клиент передает json как объект
        if (is_object($json_response)) {
            $json_response = $json_response->getContent();
        }
        $input = json_decode($json_response);

        if (!isset($input->username) || !isset($input->email) || !isset($input->text)) {
            $response = 'invalid fields (should be: username, email, text)';
            $danger = true;
            $status = 400;

        } elseif ($input->username === '' || $input->email === '' || $input->text === '') {
            $response = 'empty fields';
            $danger = true;
            $status = 400;
        }

        if ($status == 201) {
            //todo сделать обработку возможных ошибок
            $new_entry = Entry::create([
                'username' => $input->username,
                'email' => $input->email,
                'text' => $input->text,
            ]);
        }

        $data['danger'] = $danger;
        $data['response'] = $response;
        $data['status'] = $status;

        return $data;
    }

    public function updateEntry($json_response)
    {
        $status = 201;
        $danger = false;
        $response = 'success';

        if (is_object($json_response)) {
            $json_response = $json_response->getContent();
        }
        $input = json_decode($json_response);

        if (!isset($input->id) || !isset($input->text)) {
            $response = 'invalid fields (should be: id, text)';
            $danger = true;
            $status = 400;

        } elseif ($input->id === '' || $input->text === '') {
            $response = 'empty fields';
            $danger = true;
            $status = 400;
        }

        if ($status == 201) {

            //todo сделать обработку возможных ошибок
            $entry = Entry::find($input->id);
            $entry->text = $input->text;
            $entry->save();

        }

        $data['danger'] = $danger;
        $data['response'] = $response;
        $data['status'] = $status;

        return $data;
    }

    public function deleteEntry($json_response)
    {
        $status = 200;
        $danger = false;
        $response = 'success';

        if (is_object($json_response)) {
            $json_response = $json_response->getContent();
        }
        $input = json_decode($json_response);

        if (!isset($input->id)) {
            $response = 'invalid fields (should be: id)';
            $danger = true;
            $status = 400;

        } elseif ($input->id === '') {
            $response = 'empty fields';
            $danger = true;
            $status = 400;
        }

        if ($status == 200) {
            $entry = Entry::find($input->id);      ;
            if (is_null($entry)) {
                $response = 'Entry not found or already deleted';
                $danger = true;
                $status = 404;
            } else {
                $entry->delete();
            }
        }

        $data['danger'] = $danger;
        $data['response'] = $response;
        $data['status'] = $status;

        return $data;
    }


}